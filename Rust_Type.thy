theory Rust_Type
imports Main "$AFP/IEEE_Floating_Point/IEEE" Word
begin

chapter "Rust Types"

text \<open>
This is a model of the basic features of Rust's type system.
Not all of the details modeled are relevant to MIR, and those bits will be pared down
as I discover them.
\<close>

datatype ty_param = TypeSpace nat | SelfSpace nat | FnSpace nat

section "Region System"

text \<open>
These types model Rust's region system.
\<close>

datatype bound_region 
  = BrAnon nat 
  | BrNamed nat 
  | BrFresh nat 
  | BrEnv

datatype code_extent 
  = Misc nat 
  | CallSiteScope nat nat 
  | ParameterScope nat nat 
  | DestructionScope nat 
  | BlockRemainder nat nat

datatype region
  = ReEarlyBound ty_param nat
  | ReLateBound nat bound_region
  | ReFree code_extent bound_region
  | ReScope code_extent
  | ReStatic
  | ReVar nat
  | ReSkolemized nat bound_region
  | ReEmpty
  | ReErased

section "Algebraic Data Types"

text \<open>
This section contains the model of Rust's algebraic data types.
In Rust, algebraic data types are a unification of a few different surface syntaxes. In particular,
it is a unification of enum and struct types.
\<close>

datatype vis = Priv | Pub

text \<open>
@{typ vis} indicates the visibility of an ADT field. This has some interesting implications on
the availability of certain constructs outside of certain modules.
\<close>

record 'a field_def =
  visibility :: vis
  type :: "'a option"

text \<open>
A field of a variant of an ADT.
\<close>

copy_bnf ('a, 'z) field_def_ext


datatype variant_kind = VarStruct | VarTuple | VarUnit

text \<open>
ADTs have three kinds of variants: struct-like, tuple-like, and unit-like.
These impact the construction/destruction syntax used for that particular variant, but otherwise
doesn't affect the semantics.
\<close>

record 'a variant_def =
  discr :: nat
  fields' :: "('a field_def) list"
  kind :: variant_kind

text \<open>
A variant of an ADT has a discriminant which is used to determine which variant of an ADT is
represented by a given value. Additionally, it has a list of fields for the data it carries.
\<close>

copy_bnf ('a, 'z) variant_def_ext

datatype adt_flag
  = IS_ENUM 
  | IS_DTORCK 
  | IS_DTORCK_VALID 
  | IS_PHANTOM_DATA 
  | IS_SIMD 
  | IS_FUNDAMENTAL 
  | IS_NO_DROP_FLAG

text \<open>
@{typ adt_flag}s are used to indicate constraints on an ADT to the compiler.
For example, the @{term IS_SIMD} @{typ adt_flag} is used to indicate that the type represents a
packed vector of values to the LLVM backend.
\<close>

record 'a adt_def =
  variants :: "('a variant_def) list"
  flags :: "adt_flag set"
  sized_constraint :: "'a option"

text \<open>
An ADT is a list of the variants which define it, as well as flags which influence how it is
compiled. A struct will have a single adt_def.
\<close>

copy_bnf ('a, 'z) adt_def_ext

datatype unsafety = Safe | Unsafe

text \<open>
@{typ unsafety} is Rust's primitive effect system, where only code marked @{term Unsafe} is
permitted to call other @{term Unsafe} code.
\<close>

datatype Abi = RustAbi | SystemAbi | CAbi | RustCallAbi | OtherAbi nat

text \<open>
@{typ Abi} is used for annotating the function-call discipline that a given function expects.
This is mostly used for FFI.
\<close>

section "Function types"

datatype 'a fn_output
  = FnConverging 'a
  | FnDiverging -- "written ! in the surface syntax"


record 'a fn_sig =
  inputs :: "'a list"
  output' :: "'a fn_output"
  variadic :: bool

copy_bnf ('a, 'z) fn_sig_ext

datatype 'a fn_kind
  = FnDef nat "ty_param \<rightharpoonup> 'a"
  | FnPtr

record 'a fn_type =
  kind :: "'a fn_kind"
  unsafety :: unsafety
  sig :: "'a fn_sig"

copy_bnf ('a, 'z) fn_type_ext

section "Traits"

datatype 'a polybinder = Polybinder 'a -- "for<'l> a"

text {*
The @{type polybinder} type represents the binder of a higher-ranked lifetime, 
@{verbatim "for<'l> T"} in Rust's concrete syntax, where @{verbatim "T"} is a closure, function,
trait reference, or 
*}

datatype 'a param_space_data = ParamSpaceData (types: "'a list") (selfs: "'a list") (fns: "'a list")

type_synonym 'a trait_ref = "nat \<times> ty_param \<rightharpoonup> 'a"

text {*
References to traits take the form of @{type trait_ref}, which is a pair of name and substitutions
for type parameters of the trait.
*}

record 'a projection_ty =
  ref_name :: nat
  ref_substs :: "ty_param \<rightharpoonup> 'a"
  item_name :: nat

copy_bnf ('a, 'z) projection_ty_ext

text {*
A @{type projection_ty} is the projection of an associated type from a trait reference.  
*}


datatype builtin_bound = Send | Sized | Copy | Sync

text \<open>
The @{typ builtin_bound}s are considered specially by the type system. For example, a type
which is @{term Sized} may have its values stored in local variables. Uses of values of a type which
is @{term Copy} are not considered to be moves for the purpose of ownership transfer.
\<close>

record 'a trait =
  name :: nat
  substs :: "ty_param \<rightharpoonup> 'a"
  hrlt :: nat -- "higher ranked lifetime name"
  bound_region :: region
  builtin_bounds :: "builtin_bound set"
  projection_bounds :: "('a \<times> 'a projection_ty polybinder) list"

copy_bnf ('a, 'z) trait_ext


section "Tying it all together"


datatype mut = Mut | Const

text \<open>
@{typ mut} has a dual purpose: to select a kind of reference, or annotate raw pointers with
their intended usage.
\<close>

datatype primtype
  = TyBool
  | TyNumeric

text \<open>
The primitive types include booleans and lots of numeric types. We ignore these for the moment to
focus on the more interesting cases.
\<close>


type_synonym ptr = "32 word"

datatype rty
  = TyPrim primtype
  | TyEnum "rty adt_def"
  | TyStruct "rty adt_def"
  | TyBox rty
  | TyArray rty ptr -- "[T; n]"
  | TySlice rty
  | TyRawPtr mut rty
  | TyRef region mut rty
  | TyFn "rty fn_type"
  | TyBareFn unsafety nat "rty fn_sig"
  | TyTrait nat
  | TyTup "rty list"
(*  | TyParam nat *)
(*  | TyErr *)
datatype substs
  = Substs "rty param_space_data" "region param_space_data"

section "Values"

type_synonym closure_substs = unit

type_synonym ptr = "32 word"

datatype primval
  = PValBool bool
  | PValChar nat
  | PValInt "32 word"
  | PValUInt "32 word"
  | PValFloat IEEE.format IEEE.representation
  | PValRawPtr ptr
  | PValRef ptr
  | PValFnPtr ptr
  | PValStr ptr ptr

text \<open>
Numeric values right now are too abstract to be correct - the different word sizes should be
separate, and the two different sizes of floats.
\<close>

datatype val
  = ValPrim primval
  | ValEnum "nat \<times> (nat \<rightharpoonup> val)"
  | ValStruct "nat \<rightharpoonup> val"
  | ValBox ptr
  | ValArray "val list"
  | ValSlice ptr ptr ptr -- "Slice of memory at 0 starting at idx 1 ending at idx 2"
  | ValTrait "nat \<times> (nat \<rightharpoonup> val)"
  | ValTup "val list"
(*  | ValParam nat
  | ValErr *)

text \<open>
ValErr is somewhat mysterious. ValParam refers to function arguments? Not sure what it's there for.
The rest are pretty straightforward. Enums are a pair of tag and a field map. Structs are a field
map. Traits are, currently, tag and function mapping. The values mapped to should all be
ValPrim PValFnPtr, though.
\<close>

end
