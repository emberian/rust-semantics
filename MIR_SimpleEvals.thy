theory MIR_SimpleEvals
imports MIR_Syntax MIR_State Utils
begin

fun
  current_frame :: "state \<Rightarrow> frame option"
where
  "current_frame s = (case (stack s) of
      f # fs \<Rightarrow> Some f
    | [] \<Rightarrow> None)"

type_synonym heap = "ptr \<Rightarrow> 8 word"

fun
  read_prim :: "heap \<Rightarrow> primtype \<Rightarrow> ptr \<Rightarrow> primval"
where
  "read_prim h TyBool p = (if (h p) = 0 then PValBool True else PValBool False)"
| "read_prim h TyNumeric p = PValInt 0"

fun
  read_ptr :: "heap \<Rightarrow> ptr \<Rightarrow> ptr"
where
  "read_ptr h p = (ucast $ h 0 << 24) OR (ucast $ h 1 << 16) OR (ucast $ h 2 << 8) OR (ucast $ h 0)"

fun size_of :: "rty \<Rightarrow> 32 word"
where "size_of t = 1"

fun align_of :: "rty \<Rightarrow> 32 word"
where "align_of t = 1"

definition ptr_size[simp]: "ptr_size \<equiv> 4"

function (sequential)
    read_val :: "state \<Rightarrow> rty \<Rightarrow> ptr \<Rightarrow> val option"
and read_adt :: "state \<Rightarrow> bool \<Rightarrow> rty adt_def \<Rightarrow> ptr \<Rightarrow> val option"
and read_array :: "state \<Rightarrow> rty \<Rightarrow> ptr \<Rightarrow> ptr \<Rightarrow> nat \<Rightarrow> val option list"
and read_tup :: "state \<Rightarrow> rty list \<Rightarrow> ptr \<Rightarrow> val option list"
where
  "read_val s (TyPrim prim) p = Some (ValPrim (read_prim (memory s) prim p))"
| "read_val s (TyEnum adt) p = read_adt s True adt p"
| "read_val s (TyStruct adt) p = read_adt s False adt p"
| "read_val s (TyBox _) p = Some (ValBox (read_ptr (memory s) p))"
| "read_val s (TyArray t n) p = map_option ValArray (loall $ read_array s t p (align_of t) (unat n))"
| "read_val s (TySlice _) p = (let h = memory s in Some (ValSlice (read_ptr h p) 
                                                                  (read_ptr h (p + ptr_size)) 
                                                                  (read_ptr h (p + ptr_size*2))))"
| "read_val s (TyRawPtr _ _) p = Some (ValPrim (PValRawPtr (read_ptr (memory s) p)))"
| "read_val s (TyRef _ _ _) p =  Some (ValPrim (PValRef (read_ptr (memory s) p)))"
| "read_val s (TyFn _) p =  Some (ValPrim (PValFnPtr (read_ptr (memory s) p)))"
| "read_val s (TyBareFn _ fnnum _) _ = map_option (ValPrim \<circ> PValFnPtr) (barefns s $ fnnum)"
| "read_val s (TyTrait traitnum) p = None"
| "read_val s (TyTup tys) p = map_option ValTup (loall $ read_tup s tys p)"
| "read_adt is_enum s adt p = None"
| "read_array s t p align 0 = []"
| "read_array s t p align (Suc n) = (read_val s t p) # read_array s t (p + align) align n"
| "read_tup s (t # tys) p = (read_val s t p) # read_tup s tys (p + align_of t)"
| "read_tup s [] p = []"
by pat_completeness auto
termination by size_change

(* TODO: replace option here with an exception monad, because CheckedBinaryOp can trigger unwinding *)

fun adt_field_offset :: "nat \<Rightarrow> rty \<Rightarrow> ptr"
where
  "adt_field_offset n t = 0"

fun rvalue_num :: "val \<Rightarrow> ptr option"
where
  "rvalue_num (ValPrim (PValInt v)) = Some v"
| "rvalue_num (ValPrim (PValUInt v)) = Some v"
| "rvalue_num _ = None"

fun lval_get_len :: "lval \<Rightarrow> ptr option"
where
  "lval_get_len (_, LvalExtraLength l) = Some l"
| "lval_get_len _ = None"

fun with_operand_type :: "operand \<Rightarrow> rty \<Rightarrow> operand"
where
  "with_operand_type (Consume _ lvalue) t = Consume t lvalue"
| "with_operand_type (Constant _ literal) t = Constant t literal"

fun eval_binop :: "binop \<Rightarrow> val \<Rightarrow> val \<Rightarrow> val option"
where
  "eval_binop _ _ _ = None"

fun eval_unop :: "unop \<Rightarrow> val \<Rightarrow> val option"
where
  "eval_unop _ _ = None"

fun make_aggregate :: "aggregate_kind \<Rightarrow> val list \<Rightarrow> val option"
where
  "make_aggregate _ _ = None"

function (sequential)
    eval_lvalue :: "state \<Rightarrow> lvalue \<Rightarrow> lval option"
and eval_rvalue :: "state \<Rightarrow> rvalue \<Rightarrow> val option"
and read_operand :: "state \<Rightarrow> operand \<Rightarrow> val option"
where
  "eval_lvalue s (Var n) = map_option (\<lambda>vn. (vn, LvalExtraNone)) (Option.bind (current_frame s) (\<lambda>f. nth_opt (args f) n))"
| "eval_lvalue s (Temp n) = map_option (\<lambda>tn. (tn, LvalExtraNone)) (Option.bind (current_frame s) (\<lambda>f. nth_opt (tmps f) n))"
| "eval_lvalue s (Arg n) = map_option (\<lambda>an. (an, LvalExtraNone)) (Option.bind (current_frame s) (\<lambda>f. nth_opt (args f) n))"
| "eval_lvalue s (Static n) = map_option (\<lambda>sn. (sn, LvalExtraNone)) (nth_opt (statics s) n)"
| "eval_lvalue s ReturnPointer = map_option (\<lambda>rp. (rp, LvalExtraNone)) (Option.bind (current_frame s) (\<lambda>f. return_ptr f))"
| "eval_lvalue s (Projection (l, p)) = Option.bind (eval_lvalue s l)
 (\<lambda>lval. let lv = fst lval in case p of
    Deref \<Rightarrow> Some (read_ptr (memory s) lv, LvalExtraNone)
  | Field n t \<Rightarrow> Some (lv + (adt_field_offset n t), LvalExtraNone)
  | Index rv \<Rightarrow> map_option (\<lambda>fv. (fv + lv, LvalExtraNone)) (Option.bind (eval_rvalue s rv) rvalue_num)
  | ConstantIndex _ _ _ \<Rightarrow> None
  | Subslice start end \<Rightarrow> map_option (\<lambda>len. (lv + start, LvalExtraLength (end - start))) (lval_get_len lval)
  | Downcast t n \<Rightarrow> None)
"
| "eval_rvalue s (Use oper) = read_operand s oper"
| "eval_rvalue s (Repeat oper n) = map_option (\<lambda>val. ValArray (list.map (\<lambda>a. val) (List.upt 0 n))) (read_operand s oper)"
| "eval_rvalue s (Ref region mut' lval) = map_option (\<lambda>lval. ValPrim (PValRef (fst lval))) (eval_lvalue s lval)"
| "eval_rvalue s (Len lv) = map_option (ValPrim \<circ> PValUInt) (Option.bind (eval_lvalue s lv) lval_get_len)"
| "eval_rvalue s (Cast _ oper t) = read_operand s (with_operand_type oper t)"
| "eval_rvalue s (BinaryOp binop oper1 oper2) = Option.bind (read_operand s oper1) 
    (\<lambda>val1. Option.bind (read_operand s oper2) (\<lambda>val2. eval_binop binop val1 val2))"
(* TODO: actually do checking *)
| "eval_rvalue s (CheckedBinaryOp binop oper1 oper2) = eval_rvalue s (BinaryOp binop oper1 oper2)"
| "eval_rvalue s (Aggregate agkind opers) = Option.bind (loall (map (read_operand s) opers)) 
    (make_aggregate agkind)"
| "eval_rvalue s (UnaryOp unop oper) = Option.bind (read_operand s oper) (eval_unop unop)"
| "read_operand s (Constant t (Value v)) = Some v"
| "read_operand s (Constant t (Promoted idx)) =
  Option.bind (current_frame s) (\<lambda>f. nth_opt (promoted (frame_for f)) idx)"
| "read_operand s (Constant t (Item n sbs)) = None" (* FIXME: what is Item again? *)
| "read_operand s (Consume t lv) = Option.bind (eval_lvalue s lv) (\<lambda>lv. read_val s t (fst lv))"
by pat_completeness auto

end