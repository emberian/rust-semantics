theory MIR_Syntax
imports Main Rust_Type
begin

chapter "MIR abstract syntax"

record var_decl =
  mut :: mut
  name :: nat
  ty :: rty

text \<open>
A @{typ var_decl} represents a local variable from the source, ie, not compiler-introduced
temporaries. The name is the index into the var_decls list in the eventual mir_func.
\<close>

datatype tmp_decl = TmpDecl nat rty

text \<open>
A @{typ tmp_decl} represents a compiler-introduced temporary. These are always mutable (FIXME:
is this correct? or are they SSA?).
\<close>

datatype arg_decl = ArgDecl nat rty bool -- "type and 'spread' representing tuple collection for rust-call"

text \<open>
A @{typ arg_decl} represents an argument to the function. The bool field is a bit strange and is
an internal detail of how the trait for callables works.
\<close>

datatype upvar_decl = UpvarDecl nat bool

text \<open>
Upvars are reference to captured variables inside a closure.

It's not obvious to me if these are actually necessary for MIR? Closures should have already been
collapsed at this point I think.
\<close>


datatype 'r projection_elem
  = Deref -- "dereference the lvalue"
  | Field nat rty -- "access a field of an ADT"
  | Index 'r -- "index an array/slice"
  | ConstantIndex nat nat bool -- "don't remember what this does..."
  | Subslice ptr ptr -- "take a subslice"
  | Downcast rty nat -- "Downcast from one type to another (probably traits?)"

type_synonym ('l, 'r) projection = "'l \<times> 'r projection_elem"

datatype literal
  = Item nat substs
  | Value val
  | Promoted nat

text \<open>
Value is simple. Item is not so simple, and I'm not sure what it is anymore. Promoted is an index
into the mir_func's internal list of "promoted constants", which is a detail I think I'll avoid
modelling if Niko doesn't think it's important.
\<close>

datatype cast_kind
  = Misc
  | ReifyFnPointer -- "cast from a named function pointer to an actual function pointer"
  | UnsafeFnPointer -- "cast between safe/unsafe function pointers"
  | Unsize -- "the 'unsize' operation, which for example goes from [T; n] to [T]"

datatype aggregate_kind
  = Vec
  | Tuple
  | Adt "rty adt_def" nat substs "nat option"
  | Closure nat closure_substs

datatype binop
  = Add
  | Sub
  | Mul
  | Div
  | Rem
  | BitXor
  | BitAnd
  | BitOr
  | Shl
  | Shr
  | Eq
  | Lt
  | Le
  | Ne
  | Ge
  | Gt

datatype unop
  = Not -- "bitwise or logical depending on the type"
  | Neg

datatype lvalue
  = Var nat -- "reference to local variable"
  | Temp nat -- "reference to temporary"
  | Arg nat -- "reference to argument"
  | Static nat -- "reference to static data"
  | ReturnPointer -- "the pointer to the slot the return value will be written to"
  | Projection "(lvalue, rvalue) projection" -- "projection of some rvalue"
and rvalue
  = Use operand -- "read from a value, moving it if it's not copy"
  | Repeat operand nat -- "[expr; n]"
  | Ref region bool lvalue -- "Reference to an lvalue, with the given region and optionally mutable"
  | Len lvalue -- "Extract the length of a dynamically sized type"
  | Cast cast_kind operand rty -- "Do a cast of a value to a given type"
  | BinaryOp binop operand operand -- "Binary operation of two values"
  | CheckedBinaryOp binop operand operand -- "Checked arithmetic of two values"
  | UnaryOp unop operand -- "Unary operator of one value"
  | Aggregate aggregate_kind "operand list" -- "Construct an aggregate"
and operand
  = Consume rty lvalue -- "Read an lvalue and move"
  | Constant rty literal -- "Constant value"

datatype lval_extra
  = LvalExtraNone
  | LvalExtraLength ptr
  | LvalExtraVtable ptr
  | LvalExtraDowncastVariant ptr

type_synonym lval = "ptr \<times> lval_extra"
text \<open>
lvalues and rvalues are the expressions of this language. They follow the classic striation where
lvalues compute assignment destinations and rvalues compute things that will be stored. They
become combined when doing complex field/array access etc. Unfortunately these expressions are
not exactly pure, as CheckedBinaryOp can exceptional control flow.
\<close>

type_synonym jump_target = nat

datatype terminator 
  = Goto jump_target
  | If operand jump_target jump_target
  | Switch lvalue "rty adt_def" "jump_target list"
  | SwitchInt lvalue "rty adt_def" "val \<rightharpoonup> jump_target"
  | Resume
  | Return
  | Unreachable
  | Drop lvalue jump_target "jump_target option"
  | DropAndReplace lvalue operand jump_target "jump_target option"
  | Call operand "operand list" "(lvalue \<times> jump_target) option" "jump_target option"
  | Assert operand bool jump_target "jump_target option"

text \<open>
Terminators are the ends of basic blocks, and determine the control flow between basic blocks.
\<close>

datatype stmt 
  = Assign lvalue rvalue
  | SetDiscriminant lvalue nat

datatype basic_block = BasicBlock "stmt list" "terminator option" bool

record mir_func =
  promoted :: "val list" -- "promoted constant values"
  var_decls :: "var_decl list"
  arg_decls :: "arg_decl list"
  tmp_decls :: "tmp_decl list"
  upvar_decls :: "upvar_decl list"
  blocks :: "basic_block list"

end