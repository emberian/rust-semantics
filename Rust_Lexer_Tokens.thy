theory Rust_Lexer_Tokens
imports Main
begin

(* token types split to keep the time spent defining the datatype down.
   a single massive datatype takes some minutes. *)

datatype lit_token
 = LIT_BYTE
 | LIT_CHAR
 | LIT_INTEGER
 | LIT_FLOAT
 | LIT_STR
 | LIT_STR_RAW
 | LIT_BYTE_STR
 | LIT_BYTE_STR_RAW

datatype symbol_token
 = SHL
 | SHR
 | LE
 | EQEQ
 | NE
 | GE
 | ANDAND
 | OROR
 | SHLEQ
 | SHREQ
 | MINUSEQ
 | ANDEQ
 | OREQ
 | PLUSEQ
 | STAREQ
 | SLASHEQ
 | CARETEQ
 | PERCENTEQ
 | DOTDOT
 | DOTDOTDOT
 | MOD_SEP
 | RARROW
 | FAT_ARROW

datatype keyword_token
 = SELF
 | STATIC
 | AS
 | BREAK
 | CRATE
 | ELSE
 | ENUM
 | EXTERN
 | FALSE
 | FN
 | FOR
 | IF
 | IMPL
 | IN
 | LET
 | LOOP
 | MATCH
 | MOD
 | MOVE
 | MUT
 | PRIV
 | PUB
 | REF
 | RETURN
 | STRUCT
 | TRUE
 | TRAIT
 | TYPE
 | UNSAFE
 | USE
 | WHILE
 | CONTINUE
 | PROC
 | BOX
 | CONST
 | WHERE
 | TYPEOF

datatype token_type
  = Lit lit_token
  | Sym symbol_token
  | Kw keyword_token
  | IDENT
  | UNDERSCORE
  | LIFETIME
  | INNER_DOC_COMMENT
  | SHEBANG
  | SHEBANG_LINE
  | STATIC_LIFETIME

end