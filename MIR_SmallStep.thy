theory MIR_SmallStep
imports MIR_Syntax MIR_State
begin

fun next_bb :: "state \<Rightarrow> terminator \<Rightarrow> basic_block option"
where
"next_bb f (Goto jt) = 
  (let blocks = ((blocks o frame_for) f) in
    if jt > length blocks then None else Some (nth blocks jt))"



end