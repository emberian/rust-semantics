theory Utils
imports Main "$AFP/Word_Lib/Word_Lib"
begin

fun nth_opt :: "'a list => nat => 'a option"
where
  "nth_opt l i = (if i > length l then None else Some (nth l i))"

fun omap :: "'a option \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> 'b option"
where
  "omap (Some v) f = Some (f v)"
| "omap None f = None"

fun infix_fun_app :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a \<Rightarrow> 'b" (infixr "$" 10)
where
  "infix_fun_app a b = a b"

fun loall :: "'a option list \<Rightarrow> 'a list option"
where
  "loall [] = Some []"
| "loall (Some x # xs) = (case loall xs of Some l \<Rightarrow> Some (x # l) | None \<Rightarrow> None)"
| "loall (None # xs) = None"

end