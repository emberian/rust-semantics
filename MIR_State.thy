theory MIR_State
imports MIR_Syntax Word
begin

record frame = -- "Stack frames"
  frame_for :: mir_func -- "Function executing for this frame"
  name :: nat -- "Index into the list of funcs"
  ty_substs :: substs -- "Type substitutions for this polymorphic function?"
  return_ptr :: "ptr option" -- "Pointer that the return value will be written to"
  return_to :: "basic_block option" -- "Basic block to jump to when done (Return terminator)"
  args :: "ptr list" -- "Pointers to arguments"
  vars :: "ptr list" -- "Pointers to local variables"
  tmps :: "ptr list" -- "Pointers to temporaries"
  currently_executing :: basic_block -- "Currently executing basic block"
  stmt_idx :: nat -- "Index into the statement list of currently_executing that is currently executing"

record state =
  memory :: "32 word \<Rightarrow> 8 word" -- "Heap"
  funcs :: "32 word \<rightharpoonup> mir_func" -- "Function pointers"
  barefns :: "nat \<rightharpoonup> 32 word" -- "Map from function numbers to function pointers"
  stack :: "frame list" -- "Stack frames"
  statics :: "ptr list" -- "Static data"

end