theory Rust_Abstract_Syntax
imports Main
begin

section "Abstract syntax of Rust programs"

datatype name = name nat
datatype syntax_context = syntax_context nat
record ident =
  name :: name
  ctxt :: syntax_context

definition
  "EMPTY_CTXT \<equiv> syntax_context 0"

type_synonym nodeid = nat
type_synonym crate_num = nat

definition
  "CRATE_NODE_ID \<equiv> 0"

consts DUMMY_NODE_ID :: nodeid

record lifetime =
  id :: nodeid
  name :: name

record lifetime_def =
  lifetime :: lifetime
  bounds :: "lifetime set"

record angle_param_data =
  lifetimes :: "lifetime list"
  types :: "ty list"
  bindings :: "type_binding list"

record paren_param_data =
  inputs :: "ty list"
  "output" :: "ty option"

datatype path_parameters
  = AngleBracketed angle_param_data
  | Parenthesized paren_param_data

record path_segment =
  ident :: ident
  parameters :: path_parameters

record path =
  global :: bool
  segments :: "path_segment list"

datatype ty_param_bound 
  = Trait poly_trait_ref trait_bound_mod
  | Region lifetime

datatype trait_bound_mod
  = None
  | Maybe

type_synonym ty_param_bounds =  "ty_param_bound list"

record ty_param =
  ident :: ident
  id :: nodeid
  bounds :: ty_param_bounds
  default :: "ty option"

record generics =
  lifetimes :: "lifetime_def list"
  ty_params :: "ty_param list"
  where_clause :: where_clause

record where_clause =
  id :: nodeid
  preds :: "where_pred list"

datatype where_pred
  = Bound bound_pred
  | Region region_pred
  | Eq eq_pred

text {* for<'c> Foo: Send+Clone+'c *}

record bound_pred =
  bound_lifetimes :: "lifetime_def list"
  bounded_ty :: ty
  bounds :: ty_param_bounds

text {* 'a: 'b + 'c *}

record region_pred =
  lifetime :: lifetime
  bounds :: "lifetime list"

text {* T=int *}

record eq_pred =
  id :: nodeid
  path :: path
  ty :: ty

type_synonym crate_config = "meta_item list"

record crate =
  module :: module
  config :: crate_config
  exported_macros :: "macro_def list"

datatype meta_item
  = Word string
  | List string "meta_item list"
  | NameValue string lit

record block =
  stmts :: "stmt list"
  expr :: "expr option"
  id :: nodeid
  rules :: block_check_mode

record pat =
  id :: nodeid
  node :: pat_kind

record field_pat =
  ident :: ident
  pat :: pat
  is_shorthand :: bool

datatype binding_mode 
  = ByRef mutability
  | ByValue mutability

datatype PatKind
  = Wild
  | Ident binding_mode ident "pat option"
  | Struct path "field_pat list" bool
  | TupleStruct path "pat list" "nat option"
  | Path path
  | QPath qself path
  | Tuple "pat list" "nat option"
  | Box pat
  | Ref pat mutability
  | Lit expr
  | Range expr expr
  | Vec "pat list" "pat option" "pat list"
  | Mac unit

datatype mutability
  = Mutable
  | Immutable

datatype binop
  = Add
  | Sub
  | Mul
  | Div
  | Rem
  | And
  | Or
  | BitXor
  | BitAnd
  | BitOr
  | Shl
  | Shr
  | Eq
  | Lt
  | Le
  | Ne
  | Ge
  | Gt

definition
  binop_lazy_def[simp]:
  "binop_lazy opr \<equiv> opr \<in> {And, Or}"

definition
  binop_shift_def[simp]:
  "binop_shift opr \<equiv> opr \<in> {Shl, Shr}"

definition
  binop_comparison_def[simp]:
  "binop_comparison opr \<equiv> opr \<in> {Eq, Lt, Le, Ne, Gt, Ge}"

definition
  binop_by_value_def[simp]:
  "binop_by_value opr \<equiv> \<not> binop_comparison opr"

datatype unop
  = Deref
  | Not
  | Neg

datatype stmt
  = Decl decl nodeid
  | Expr expr nodeid
  | Semi expr nodeid
  | Mac unit

record local =
  pat :: pat
  ty :: "ty option"
  init :: "expr option"
  id :: nodeid
  attrs :: thin_attrs

datatype decl
  = Local local
  | Item item

record arm =
  attrs :: "attribute list"
  pats :: "pat list"
  guard :: "expr option"
  

end